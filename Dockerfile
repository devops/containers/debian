ARG base_image=buster
FROM debian:${base_image}

RUN set -eux; \
	apt-get -y update; \
	apt-get -y upgrade; \
	apt-get -y install \
  	  less \
 	  locales \
	  locales-all \
	  nano \
 	  ; \
	apt-get -y clean; \
	rm -rf /var/lib/apt/lists/*

ENV LANG     en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL   en_US.UTF-8
ENV TZ       US/Eastern

RUN locale-gen $LANG
